<?php
declare(strict_types=1);

namespace MVQN\Slim\Middleware\Authentication\Authenticators;

use Psr\Http\Server\MiddlewareInterface;

/**
 * Class Authenticator
 *
 * @package MVQN\Slim\Middleware\Authentication\Authenticators
 *
 * @author Ryan Spaeth
 * @copyright 2020 Spaeth Technologies, Inc.
 */
abstract class Authenticator implements MiddlewareInterface
{
}
